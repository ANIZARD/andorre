function submitUForm(formID){
  var inputs = document.getElementById(formID).getElementsByTagName("input");
  var textareas = document.getElementById(formID).getElementsByTagName("textarea");
  var datas = new FormData();
  for (input of inputs){
    if (input.name){
      datas.append(input.name,input.value);
    }
  }
  for (textarea of textareas){
    if (textarea.name){
      datas.append(textarea.name,textarea.value);
    }
  }
  axios({
    method: 'post',
    url:'/wp-admin/admin-ajax.php',
    data: datas,
    config: {headers: {'Content-Type': 'multipart/form-data' }}
  })
  .then((r) =>{
    //console.log(r);
    document.getElementById(formID).getElementsByClassName('message')[0].innerHTML = r.data.data.message;
    var pouet=document.getElementById(formID).elements;
    for (var i = 0, len = pouet.length; i < len; ++i) {
      pouet[i].readOnly = true;
      pouet[i].disabled = true;
    }
    //facebook
    fbq('track', 'CompleteRegistration', {
      value: 1,
      currency: 'eur',
    });
    if (typeof ga === "function"){
      ga('send', 'event', 'recup', 'mail');
      console.log("mail ok");
    }
  })
  .catch((e) => {
    document.getElementById(formID).getElementsByClassName('message')[0].innerHTML = e;
  });
};
